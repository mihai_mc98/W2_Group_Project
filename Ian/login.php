<!DOCTYPE html>
<html>
 	<head>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<meta name="author" content="Ian Vaughan"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title></title>
  	</head>
  	<body>
<?php /* ?>
taken from https://www.w3schools.com/php/php_form_required.asp
$nameErr = $name = $email = $emailErr = $validation = "";

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
    		if (empty($_POST["name"])) {
        		$nameErr = "Name is required";	
			$validation = "false";
    		}
    		else {
        		$name = $_POST["name"];
   		 }
		if (empty($_POST["email"])) {
        		$emailErr = "Email is required";
			$validation = "false";
    		}
    		else {
        		$email= $_POST["email"];
   		 }
	}
<?php // */ ?>
		<h1 class="title">Login</h1>
		<form action="welcome.php" method="post" class="centre">
			Name: <input type="text" name="name" value="<?php //echo htmlspecialchars($name);?>">
				<?php //echo $nameErr ?><br>
			E-mail: <input type="text" name="email" value="<?php //echo htmlspecialchars($email);?>">
				<?php //echo $emailErr ?><br>
		<input type="submit"/>
            <?php /* ?>		
			if ($validation =="") {
				header('Location: welcome.php');
				exit();
			} <?php // */ ?>

  	</body>
</html>
