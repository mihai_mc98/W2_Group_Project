<html>
<body>
<?php $name = $_POST["name"];
	    $email = $_POST["email"];

	//copied from https://wiki.cs.manchester.ac.uk/index.php/Web_Dashboard/Connecting_to_MySQL
	// Load the configuration file containing your database credentials
	require_once('config.inc.php');

	// Connect to the database
	$mysqli = new mysqli($database_host, $database_user, $database_pass, $database_name);

	// Check for errors before doing anything else
	if($mysqli -> connect_error) {
	    die('Connect Error ('.$mysqli -> connect_errno.') '.$mysqli -> connect_error);
	} 
	echo 'Welcome ';
	$select = $mysqli -> prepare("SELECT * FROM user WHERE name = ? AND email = ?");
	$select -> bind_param('ss', $name, $email);
	$select -> execute();

	$select -> store_result();	// store result
	$result = $select-> num_rows; //store numrows
	
	if ($result == 1) {
		echo 'back ';
	} else {
		$sql = $mysqli->prepare("INSERT INTO user (name, email) VALUES (?,?)");
		$sql -> bind_param("ss", $name, $email);
		$sql -> execute(); 
		$sql->close(); //close prepared statement
	}
	
	$select-> close(); //close prepared statement

 	echo htmlspecialchars($name); 
?>
	<br/>
	Your email address is: <?php echo htmlspecialchars($email); ?>

<?php
	
	// Always close your connection to the database cleanly!
	$mysqli -> close();
?>

</body>
</html>
